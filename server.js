var express   =    require("express");
var mysql     =    require('mysql');
var app       =    express();

const SERVER_PORT   = 8000;
var TIME_OFFSET     = -5; /*GMT -5*/
var TABLE_PREFIX    = 'ves_';

/* Database configuration*/
var pool = mysql.createPool({
    connectionLimit : 100, //important
    host     : 'localhost',
    user     : 'demo2',
    password : 'uqvWtBmru7H5m7p2',
    database : 'demo2_marketplace',
    debug    :  false
});

/* Server will serve only requests from these domains*/
const WHITE_LIST_DOMAINS = ['auction.demo2.vnecoms.com','marketplace.demo2.vnecoms.com'];

const AUCTION_STATUS_NOT_START          = 0;
const AUCTION_STATUS_PROCESSING         = 1;
const AUCTION_STATUS_FINISHED           = 2;
const AUCTION_STATUS_CLOSED             = 3;
const AUCTION_STATUS_DISABLED           = 4;

const BID_STATUS_HIGHEST    = 1;
const BID_STATUS_OVERED     = 2;
const BID_STATUS_WIN        = 3;
const BID_STATUS_LOSE       = 4;
const BID_STATUS_BOUGHT     = 5;
const BID_STATUS_EXPIRED    = 6;
const BID_STATUS_CANCELED   = 0;

 
 /**
  * Get Table name with prefix
  */
 function getTable(tableName){
     var tmpPrefix = tableName.substr(0, TABLE_PREFIX.length);
     if(tmpPrefix == TABLE_PREFIX) return tableName;
     return TABLE_PREFIX+ tableName;
 }
 
/*Fix warning: MaxListenersExceededWarning: Possible EventEmitter memory leak detected. 16 error listeners added. Use emitter.setMaxListeners() to increase limit*/
require('events').EventEmitter.defaultMaxListeners = 0;

/**
 * Get auction Ids
 * Make sure no injection risks
 *
 * @return array
 */
function getAuctionIds(ids){
    var result = [];
    var tmpIds = ids.split(',');
    for(var i = 0; i < tmpIds.length; i ++){
        if(isNaN(tmpIds[i])) continue;
        result.push(tmpIds[i]);
    }
    
    return result;
}

/**
 * Update highest bidder name to result
 */
function updateHighestBidderName(connection, auction, res, resJson, check){
    /*Get highest bidder name*/
    sql = 'SELECT bid_id, '+getTable('ves_auction_config')+'.bidder_name FROM `'+getTable('ves_auction_bid')+'` ';
    sql += 'INNER JOIN '+getTable('ves_auction_config')+' on '+getTable('ves_auction_bid')+'.customer_id = '+getTable('ves_auction_config')+'.customer_id ';
    sql += 'WHERE auction_id='+auction.auction_id+' ORDER BY bid_id DESC limit 1';
    connection.query(sql,function(err,rows){
        if(!err){
            if(rows.length){
                resJson.a[auction.auction_id].bi = rows[0].bidder_name;
            }
            var index = check.bidder_name.indexOf(auction.auction_id+'');
            if(index >= 0){
                check.bidder_name.splice(index,1);
            }
            if(!check.bidder_name.length && check.winner_name.length){
                connection.release();
                responseUpdate(res, resJson);
            }
        }else{
            res.json({'eror':err});
        }
    });
}

/**
 * Update winner name to result
 */
 
function updateWinnerName(connection, auction, res, resJson, check){
    sql = 'SELECT bid_id, '+getTable('ves_auction_config')+'.bidder_name FROM '+getTable('ves_auction_bid')+' ';
    sql += 'INNER JOIN '+getTable('ves_auction_config')+' on '+getTable('ves_auction_bid')+'.customer_id = '+getTable('ves_auction_config')+'.customer_id ';
    sql += 'WHERE auction_id='+auction.auction_id+' AND '+getTable('ves_auction_bid')+'.status in('+[BID_STATUS_WIN, BID_STATUS_BOUGHT, BID_STATUS_EXPIRED].join(',')+')';
    sql += 'ORDER BY bid_id DESC';
    connection.query(sql,function(err,rows){
        if(!err){
            if(rows.length){
                var winners = [];
                for(var i = 0; i < rows.length; i ++){
                    winners.push(rows[i].bidder_name);
                }
                resJson.a[auction.auction_id].wi = winners;
            }
            var index = check.winner_name.indexOf(auction.auction_id+'');
            if(index >= 0){
                check.winner_name.splice(index,1);
            }
            if(!check.bidder_name.length && check.winner_name.length){
                connection.release();
                responseUpdate(res, resJson);
            }
        }else{
            res.json({'eror':err});
        }
    });
}
/**
 * Process update action
 */
function processUpdate(req,res) {

    pool.getConnection(function(err,connection){
        if (err) {
            connection.release();
            res.json({"code" : 100, "status" : "Error in connection database"});
            return;
        }
        
        var ids = getAuctionIds(req.query.ids);
        var k = req.query.k;
        var currentTime = getTimestamp();
        var check = {
            bidder_name: [].concat(ids),
            winner_name: [].concat(ids)
        };
        var resJson = {
            a: {},
            /*bn: '',*/
            /*il: false,*/
            k: k
        };

        var sql = 'SELECT '+getTable('ves_auction')+'.*, max('+getTable('ves_auction_bid')+'.price) as highest_bid_price, count('+getTable('ves_auction_bid')+'.bid_id) as bid_count ';
            sql += 'FROM `'+getTable('ves_auction')+'` ';
            sql += 'LEFT JOIN '+getTable('ves_auction_bid')+' ON '+getTable('ves_auction')+'.auction_id='+getTable('ves_auction_bid')+'.auction_id ';
            /*sql += 'INNER JOIN ves_auction_config ON ves_auction_bid.customer_id=ves_auction_config.customer_id '*/
            sql += 'WHERE '+getTable('ves_auction')+'.auction_id in ('+ ids.join(',')+') ';
            sql += 'GROUP BY '+getTable('ves_auction')+'.auction_id';

        connection.query(sql,function(err,rows){
            if(!err) {
                for(var i = 0; i < rows.length; i ++){
                    var auction = processAuctionData(rows[i]);
                    var minBidPrice = auction.bid_count > 0 ?auction.highest_bid_price + auction.min_bid_increment:auction.init_price;
                    var endTime = Math.floor(new Date(auction.end_time).getTime()/1000);
                    var timeLeft = endTime - currentTime;
                    resJson.a[auction.auction_id] = {
                        ai: auction.auction_id,
                        bc: auction.bid_count,
                        bi: "",
                        cp: auction.highest_bid_price,
                        ia: isActiveAuction(auction),
                        mbp: minBidPrice,
                        tl: timeLeft,
                        wi: ""
                    };
                    updateHighestBidderName(connection, auction, res, resJson, check);
                    updateWinnerName(connection, auction, res, resJson, check);
                }
            }else{
                res.json({'eror':err});
            }

        });
        
        connection.on('error', function(err) {      
            res.json({"code" : 100, "status" : "Error in connection database"});
            return;     
        });
   });
}

/**
 * Is active auction
 *
 * @return boolean
 */
function isActiveAuction(auction){
    var now = getTimestamp();
    var endTime = getTimestamp(auction.end_time);
    var startTime = getTimestamp(auction.start_time);
    var result = auction &&
        auction.auction_id &&
        auction.status == AUCTION_STATUS_PROCESSING &&
        startTime <= now &&
        endTime >= now;
    
    if(
        !result &&
        auction &&
        auction.status == AUCTION_STATUS_PROCESSING
    ){
        /* Set the auction as finished. */
        //finishAuction(auction);
    }
    
    return result;
}

/**
 * Finish Auction
 */
function finishAuction(auction){
    pool.getConnection(function(err,connection){
    });
}

/**
 * Get current timestamp
 * @return int
 */
function getTimestamp(time=false){
    var currentTime = time?Math.floor(new Date(time).getTime()/1000):Math.floor(new Date().getTime()/1000);
    currentTime += TIME_OFFSET * 3600;
    
    return currentTime;
}

/**
 * Response update action
 */
function responseUpdate(res, result){
    res.set("Connection", "close");
    res.json(result);
}

/**
 * Process auction data
 */
function processAuctionData(auction){
    var numberFields = [
        'init_price',
        'reserve_price',
        'current_price',
        'min_bid_increment',
        'max_bid_increment',
        'extended_time',
        'multiple_winner',
        'min_qty',
        'max_qty',
        'winner_expiration_day',
        'can_buy',
        'autobid',
        'is_featured',
        'status',
        'highest_bid_price',
        'bid_count'
    ];
    for(var i = 0; i < numberFields.length; i ++){
        if(auction[numberFields[i]]){
            auction[numberFields[i]] = parseFloat(auction[numberFields[i]]);
        }
    }
    return auction;
}

app.get("/update",function(req,res){
    try{
        if(!req.xhr && req.headers.accept.indexOf('json') == -1) {
            res.status(404).send({ error: 'You are not allowed to send this request!' });
            return;
        };
        
        for(var i=0;i<WHITE_LIST_DOMAINS.length;i++){
            var origin = WHITE_LIST_DOMAINS[i];
            if(req.headers.origin.indexOf(origin) > -1){ 
                 res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
                 break;
            }
        }
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
        res.setHeader('Access-Control-Allow-Credentials', true); // If needed
        res.setHeader('x-powered-by', 'Vnecoms-Nodejs-Express'); // If needed
        processUpdate(req,res);
    }catch(err){
        res.status(500).send({ error: 'An internal server error occurred.' });
    }
});

app.listen(SERVER_PORT);